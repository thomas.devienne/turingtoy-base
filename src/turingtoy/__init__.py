from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:

    # on déclare nos trois variable de sorties
    output = list(input_) # on travaille avec une liste et on renvoie une string
    history = list()
    isaccepted = True

    # on récupère notre état actuel et on défini le curseur
    curr_state = machine["start state"]
    cursor = 0
    curr_sym = str()
    curr_action = dict()
    next_move = str()

    # on commence notre itération
    while curr_state not in machine["final states"] and isaccepted:
        # on lis le symbole et l'action courante
        curr_sym = output[cursor]
        curr_action = machine["table"][curr_state][curr_sym]
        
        # on crée l'élément de log pour cet tour de bloucle
        history.append({
            "state": curr_state,
            "reading": curr_sym,
            "position": cursor,
            "memory": "".join(output)
        })

        # si on bouge simplement le curseur
        if isinstance(curr_action, str):
            next_move = curr_action
            history[-1]["transition"] = next_move

        # sinon on traite les différents paramètres possibles
        if isinstance(curr_action, dict):
            history[-1]["transition"] = dict()

            # récupération de l'action à faire
            if "write" in curr_action:
                curr_sym = curr_action["write"]
                output[cursor] = curr_sym
                history[-1]["transition"]["write"] = curr_sym

           # récupération de la prochaine action
            if "L" in curr_action:
                next_move = "L"
                curr_state = curr_action["L"]
                history[-1]["transition"]["L"] = curr_state
            if "R" in curr_action:
                next_move = "R"
                curr_state = curr_action["R"]
                history[-1]["transition"]["R"] = curr_state
        
        # on déplace le curseur
        if next_move == "L":
            cursor -= 1
            # on ajoute un élément à la liste
            if cursor < 0:
                output.insert(0, machine["blank"])
                cursor = 0
        elif next_move == "R":
            cursor += 1
            # on ajoute un élément à la liste si necessaire
            if cursor + 1 > len(output):
                output.append(machine["blank"])

        # on soustrait la transition du nombre d'étapes à effectuer
        # si cette dernière est précisée explicitement par l'utilisateur
        if steps != None and steps > 0:
            steps -= 1
            # si le nombre d'étapes est écoulé, alors on arrête la machine
            if steps <= 0:
                isaccepted = False

    return ("".join(output).strip(), history, isaccepted)

